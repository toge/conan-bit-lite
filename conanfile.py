import os
from conans import ConanFile, tools


class BitLiteConan(ConanFile):
    name = "bit-lite"
    version = "0.1.0"
    license = "BSL-1.0"
    author = "toge.mail@gmail.com"
    url = ""
    homepage = "https://github.com/martinmoene/bit-lite"
    description = "bit-lite - C++20 bit operations for C++98 and later in a single-file header-only library"
    topics = ("cpp20", "header-only", "bit")
    no_copy_source = True
    # No settings/options are necessary, this is header only

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get("https://github.com/martinmoene/bit-lite/archive/{}.zip".format(self.version))
        os.rename("bit-lite-{}".format(self.version), "bit-lite")

    def package(self):
        self.copy("*.hpp", "include", src="bit-lite/include")
