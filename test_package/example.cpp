#include <iostream>
#include "nonstd/bit.hpp"

int main() {
    std::cout
        << "Consecutive ones at the right in 0x17: " << nonstd::countr_one( 0x17u )
        << "\nBit width of 0x13: " << nonstd::bit_width( 0x13u ) << '\n';
    return 0;
}
